from django.test import TestCase
from django.urls import reverse
from django.test import TestCase

from rest_framework import status
from rest_framework.test import APITestCase

from .models import Regions
from .serializer import RegionsSerializer

# Create your tests here.

class RegionsApiTest(APITestCase):
    def test_region_list(self):
        REGION_URL = reverse('region-list')
        response = self.client.get(REGION_URL, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)


